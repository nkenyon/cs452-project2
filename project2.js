var gl;
var myShaderProgram;
var canvas;

var state_d4 = "idle";
var counter_d4 = 0;
var state_d6 = "idle";
var counter_d6 = 0;
var state_d8 = "idle";
var counter_d8 = 0;


function initGL(){
    canvas = document.getElementById( "gl-canvas" );
    
    gl = WebGLUtils.setupWebGL( canvas );
    if ( !gl ) { alert( "WebGL isn't available" ); }
    
    gl.enable(gl.DEPTH_TEST);
    gl.viewport( 0, 0, 512, 512 );
    gl.clearColor( 0.0, 0.0, 0.0, 1.0 );
    
    myShaderProgram = initShaders( gl, "vertex-shader", "fragment-shader" );
    gl.useProgram( myShaderProgram );  
    
    setupViewing();

    // Setup all objects here
    setup_d4();
	setup_d6();
	setup_d8();
   // setup_p();
	setup_desk();
    // End Object Setup

    setupLighting();
    
    // render the objects
    render();
	
	showPerspective(); //Show perspective view to start
};


function getFaceNormals( vertices, indexList, numTriangles ) {
    // array of face normals
    var faceNormals = [];
    var faceNormal = [];
    
    // Following lines iterate over triangles
    for (var i = 0; i < numTriangles; i++) {
        // Following lines give you three vertices for each face of the triangle
        var p0 = vec3(vertices[indexList[3*i]][0],
                      vertices[indexList[3*i]][1],
                      vertices[indexList[3*i]][2]);
        //console.log(i + "|p0= " + p0);
        
        var p1 = vec3(vertices[indexList[3*i+1]][0],
                      vertices[indexList[3*i+1]][1],
                      vertices[indexList[3*i+1]][2]);
        //console.log(i + "|p1= " + p1);

        var p2 = vec3(vertices[indexList[3*i+2]][0],
                      vertices[indexList[3*i+2]][1],
                      vertices[indexList[3*i+2]][2]);
        //console.log(i + "|p2= " + p2);
        
        // Calculate vector from p0 to p1 ( use subtract function in MV.js, NEEDS CODE )
        var p1minusp0 = vec3( p1[0]-p0[0], p1[1]-p0[1], p1[2]-p0[2]);
        //console.log(i + "|p1minusp0=" + p1minusp0);
        var p2minusp0 = vec3( p2[0]-p0[0], p2[1]-p0[1], p2[2]-p0[2]);
        //console.log(i + "|p2minusp0=" + p2minusp0);
        var faceNormal = cross(p1minusp0, p2minusp0);
        //console.log(i + "|faceNormal=" + faceNormal);
        // Avoid the stupid NaN error when normalizing a zero vector
        if(faceNormal[0]+faceNormal[1]+faceNormal[2] != 0)
            faceNormal = normalize(faceNormal);
        //console.log(i + "|faceNormalIZED=" + faceNormal);
        
        // Following line pushes the face normal into the array of face normals
        faceNormals.push( faceNormal );
    }
    
    // Following line returns the array of face normals
    return faceNormals;
}

// FOLLOWING CODE SKELETON FOR getVertexNormals() NEEDS TO BE COMPLETED
function getVertexNormals( vertices, indexList, faceNormals, numVertices, numTriangles ) {
    var vertexNormals = [];
    
    // Iterate over all vertices
    for ( var j = 0; j < numVertices; j++) {
        
        // Initialize the vertex normal for the j-th vertex
        var vertexNormal = vec3( 0.0, 0.0, 0.0 );
        
        // Iterate over all the faces to find if this vertex belongs to
        // a particular face
        
        for ( var i = 0; i < numTriangles; i++ ) {
            
            // The condition of the following if statement should check
            // if the j-th vertex belongs to the i-th face
            //if ( ) { // NEEDS CODE IN PARENTHESES
                
                // Update the vertex normal (NEEDS CODE)
                
            //}

            if(indexList[3*i]==j ||
            	indexList[3*i+1]== j ||
            	indexList[3*i+2]== j ){
            	
            	vertexNormal[0] = vertexNormal[0] + faceNormals[i][0];
            	vertexNormal[1] = vertexNormal[1] + faceNormals[i][1];
            	vertexNormal[2] = vertexNormal[2] + faceNormals[i][2];
            }
            
        }
        
        // Normalize the vertex normal here (NEEDS CODE)
        vertexNormal = normalize(vertexNormal);
        
        // Following line pushes the vertex normal into the vertexNormals array
        vertexNormals.push( vertexNormal );
    }
    
    return vertexNormals;
    
}

function render() {
    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT );

    // For each object:
    // 1. Update transform values if needed
    // 2. Call transform(obj#) to update the values in the shader
    // 3. Render the object with drawObject(obj#)
    
    // d4
    if(state_d4 == "rising"){
		if(counter_d4 < 100){
			d4.translate.y += .2;
			counter_d4++;
		}else{
			counter_d4 = 0;
			state_d4 = "spinning";
		}
	}
	if(state_d4 == "spinning"){
		if(counter_d4 < Math.random()*50+50){
			d4.rotation.y += .2;
			d4.rotation.x += .2;
			counter_d4++;
		}else{
			counter_d4 = 0;
			state_d4 = "falling";
			/*switch(Math.random()*4){
				case 0:
				d4.rotation.x = 0;
				d4.rotation.y = 10;
				d4.rotation.z = 0;
				break;
				case 1:
				
				break;

				case 2:
				
				break;
				case 3:
				
			}*/
			
		}
	}
	if(state_d4 == "falling"){
		if(counter_d4 < 100){
			d4.translate.y -= .2;
			counter_d4++;
		}else{
			counter_d4 = 0;
			state_d4 = "idle";
		}
	}
    transform(d4);
    drawObject(d4);

    //d6
	//d6.rotation.x += 0.01;
	//d6.rotation.y += 0.01;
	//d6.rotation.z += 0.01;
	if(state_d6 == "rising"){
		if(counter_d6 < 100){
			d6.translate.y += .2;
			counter_d6++;
		}else{
			counter_d6 = 0;
			state_d6 = "spinning";
		}
	}
	if(state_d6 == "spinning"){
		if(counter_d6 < Math.random()*50+50){
			d6.rotation.y += .2;
			d6.rotation.x += .2;
			counter_d6++;
		}else{
			counter_d6 = 0;
			state_d6 = "falling";
			/*switch(Math.random()*4){
				case 0:
				d6.rotation.x = 0;
				d6.rotation.y = 10;
				d6.rotation.z = 0;
				break;
				case 1:
				
				break;

				case 2:
				
				break;
				case 3:
				
				break;
				
				case 4:
				
				break;
				
				case 5:
			}*/
			
		}
	}
	if(state_d6 == "falling"){
		if(counter_d6 < 100){
			d6.translate.y -= .2;
			counter_d6++;
		}else{
			counter_d6 = 0;
			state_d6 = "idle";
		}
	}
    transform(d6);
    drawObject(d6);
	
	//d8
	
	if(state_d8 == "rising"){
		if(counter_d8 < 100){
			d8.translate.y += .2;
			counter_d8++;
		}else{
			counter_d8 = 0;
			state_d8 = "spinning";
		}
	}
	if(state_d8 == "spinning"){
		if(counter_d8 < Math.random()*50+50){
			d8.rotation.y += .2;
			d8.rotation.x += .2;
			counter_d8++;
		}else{
			counter_d8 = 0;
			state_d8 = "falling";
			/*switch(Math.random()*4){
				case 0:
				d8.rotation.x = 0;
				d8.rotation.y = 10;
				d8.rotation.z = 0;
				break;
				case 1:
				
				break;

				case 2:
				
				break;
				case 3:
				
				break;
				
				case 4:
				
				break;
				
				case 5:
			}*/
			
		}
	}
	if(state_d8 == "falling"){
		if(counter_d8 < 100){
			d8.translate.y -= .2;
			counter_d8++;
		}else{
			counter_d8 = 0;
			state_d8 = "idle";
		}
	}
	transform(d8);
	drawObject(d8);
	
	//pencil
	//transform(pencil);
	//drawObject(pencil);
	
	//desk
	transform(desk);
	drawObject(desk);

    // Animate
    requestAnimFrame(render);
}

function rolld4(){
	if(state_d4 == "idle"){
		state_d4 = "rising";
	}
}

function rolld6(){
	if(state_d6 == "idle"){
		state_d6 = "rising";
	}
}

function rolld8(){
	if(state_d8 == "idle"){
		state_d8 = "rising";
	}
}

function drawObject(obj){
    var indexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(obj.indexList), gl.STATIC_DRAW);
    
    var verticesBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, verticesBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(obj.vertices), gl.STATIC_DRAW);
    
    var vertexPosition = gl.getAttribLocation(myShaderProgram,"vertexPosition");
    gl.vertexAttribPointer( vertexPosition, 4, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vertexPosition );
    
    var normalsBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, normalsBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(obj.vertexNormals), gl.STATIC_DRAW);
    
    var vertexNormalPointer = gl.getAttribLocation(myShaderProgram,"vertexNormal");
    gl.vertexAttribPointer( vertexNormalPointer, 3, gl.FLOAT, false, 0, 0 );
    gl.enableVertexAttribArray( vertexNormalPointer );

    if(obj.material.hasTexture){
        var texCoordBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, texCoordBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, flatten(obj.textureCoordinates), gl.STATIC_DRAW);

        var texCoordsLoc = gl.getAttribLocation(myShaderProgram, "textureCoordinate");
        gl.vertexAttribPointer(texCoordsLoc, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(texCoordsLoc);
    }

    setObjectMaterial(obj.material);

    gl.drawElements(gl.TRIANGLES, 3 * obj.triCount, gl.UNSIGNED_SHORT, 0 );   
}