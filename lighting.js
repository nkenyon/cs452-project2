// lighting.js

// Light Flags
var light1_Flag = 1.0; // Flag Value 0=Off, 1=On
var light1_Flag_P; // Shader location of light1 flag
var light2_Flag = 1.0; // Flag Value 0=Off, 1=On
var light2_Flag_P; // Shader location of light2 flag

function setupLighting(){
    
    // Set up the first light source and send the variables
    // to the shader program

    var p1_P = gl.getUniformLocation (myShaderProgram, "p1");
    gl.uniform3f(p1_P, -45.0, 30.0, 50.0);

    // Values for the first light components

    // Ambient part of the light
    var Ia1_P = gl.getUniformLocation(myShaderProgram, "Ia1");
    gl.uniform3f(Ia1_P, 0.2,0.1,0.2);

    // Diffuse part of the light
    var Id1_P = gl.getUniformLocation(myShaderProgram, "Id1");
    gl.uniform3f(Id1_P, 1.0,0.8,1.0);

    // Specular part of the light
    var Is1_P = gl.getUniformLocation(myShaderProgram, "Is1");
    gl.uniform3f(Is1_P, 0.3,0.3,0.3);

    // Set up the second light source and send the variables
    // to the shader program (NEEDS CODE, VARIABLES DEPEND ON LIGHT TYPE)

    var p2_P = gl.getUniformLocation (myShaderProgram, "p2");
    gl.uniform3f(p2_P, 50.0, 30.0, 50.0);

    // Values for the second light components

    // Ambient part of the light
    var Ia2_P = gl.getUniformLocation(myShaderProgram, "Ia2");
    gl.uniform3f(Ia2_P, 0.1,0.2,0.2);

    // Diffuse part of the light
    var Id2_P = gl.getUniformLocation(myShaderProgram, "Id2");
    gl.uniform3f(Id2_P, 0.8,1.0,1.0);

    // Specular part of the light
    var Is2_P = gl.getUniformLocation(myShaderProgram, "Is2");
    gl.uniform3f(Is2_P, 0.1,0.1,0.1);

    var angle_P = gl.getUniformLocation(myShaderProgram, "angle");
    gl.uniform1f(angle_P, Math.PI/6);

    var falloff_P = gl.getUniformLocation(myShaderProgram, "falloff");
    gl.uniform1f(falloff_P, 5.0);

    var direction_P = gl.getUniformLocation(myShaderProgram, "direction_P");
    gl.uniform3f(direction_P, -50.0, -30.0, -50.0);
    
    // Initialize up on/off flags for the both light sources. These
    // flags should be controlled using buttons
    light1_Flag_P = gl.getUniformLocation(myShaderProgram, "light1_On");
    gl.uniform1f(light1_Flag_P, light1_Flag);
    light2_Flag_P = gl.getUniformLocation(myShaderProgram, "light2_On");
    gl.uniform1f(light2_Flag_P, light2_Flag);

    // Setup the specular flag
    specFlag_P = gl.getUniformLocation(myShaderProgram, "specFlag");
    gl.uniform1f(specFlag_P, specFlag);
}

function toggleLight1(){
	if(light1_Flag == 0.0) light1_Flag = 1.0;
	else if(light1_Flag == 1.0) light1_Flag = 0.0;
	gl.uniform1f(light1_Flag_P, light1_Flag);
}

function toggleLight2(){
	if(light2_Flag == 0.0) light2_Flag = 1.0;
	else if(light2_Flag == 1.0) light2_Flag = 0.0;
	gl.uniform1f(light2_Flag_P, light2_Flag);
}