// viewing.js

// Shader Values
var modelView_M;    // Matrix
var modelView_P;    // Uniform Location in Shader
var modelViewInv_M; // Matrix
var modelViewInvTrans_P; // Uniform Location in Shader

var transRX = {mat: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1], uni: ""};
var transRY = {mat: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1], uni: ""};
var transRZ = {mat: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1], uni: ""};
var transT =  {mat: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1], uni: ""};
var transS =  {mat: [1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1], uni: ""};

var orthoProj_M;
var perspProj_M;

// Model View Friends
var eye;
var at;
var vup;

var viewFlag = 0;
var projFlag_P = 0;

function setupViewing(){
	// Setup transform matrices
    transRX.uni = gl.getUniformLocation(myShaderProgram, "rX");
    transRY.uni = gl.getUniformLocation(myShaderProgram, "rY");
    transRZ.uni = gl.getUniformLocation(myShaderProgram, "rZ");
    transT.uni = gl.getUniformLocation(myShaderProgram, "t");
    transS.uni = gl.getUniformLocation(myShaderProgram, "s");
    gl.uniformMatrix4fv(transRX.uni, false, transRX.mat);
    gl.uniformMatrix4fv(transRY.uni, false, transRY.mat);
    gl.uniformMatrix4fv(transRZ.uni, false, transRZ.mat);
    gl.uniformMatrix4fv(transT.uni, false, transT.mat);
    gl.uniformMatrix4fv(transS.uni, false, transS.mat);

    // Define eye (use vec3 in MV.js)
    eye = vec3(40.0,80.0,120.0);
    
    // Define at point (use vec3 in MV.js)
    at = vec3(0.0,0.0,0.0);
    
    // Define vup vector (use vec3 in MV.js)
    vup = vec3(0.0,1.0,0.0);

    // Obtain n (use subtract and normalize in MV.js)
    var d = subtract(eye, at);
    var n = normalize(d);
    
    // Obtain u (use cross and normalize in MV.js)
    var k = cross(vup, n);
    var u = normalize(k);
    
    // Obtain v (use cross and normalize in MV.js)
    var l = cross(n, u);
    var v = normalize(l);
    
    // Set up Model-View matrix M and send M as uniform to shader

    modelViewInvTrans_M = [u[0], u[1], u[2], 0.0,
                          v[0], v[1], v[2], 0.0,
                          n[0], n[1], n[2], 0.0,
                          eye[0], eye[1], eye[2], 1.0
                         ];

    modelView_M = [ u[0] , v[0] , n[0] , 0.0,
                        u[1] , v[1] , n[1] , 0.0,
                        u[2] , v[2] , n[2] , 0.0,
                        -(eye[0]*u[0]) - (eye[1]*u[1]) - (eye[2]*u[2]),
                        -(eye[0]*v[0]) - (eye[1]*v[1]) - (eye[2]*v[2]),
                        -(eye[0]*n[0]) - (eye[1]*n[1]) - (eye[2]*n[2]),
                        1.0
                      ];

    modelView_P = gl.getUniformLocation(myShaderProgram, "modelView_M");
    gl.uniformMatrix4fv(modelView_P, false, modelView_M);

    modelViewInvTrans_P = gl.getUniformLocation(myShaderProgram, "modelViewInvTrans_M");
    gl.uniformMatrix4fv(modelViewInvTrans_P, false, flatten(modelViewInvTrans_M));
     
    // Step 2: Set up orthographic and perspective projections

    // Define left plane
    var left = -50.0;
    // Define right plane
    var right = 50.0;
    // Define top plane
    var top = 50.0;
    // Define bottom plane
    var bottom = -50.0;
    // Define near plane
    var near = 90.0;
    // Define far plane
    var far = 190.0;

    // Set up orthographic projection matrix P_orth using above planes
    orthoProj_M = [
                        2.0/(right-left), 0.0, 0.0, 0.0,
                        0.0, 2/(top-bottom), 0.0, 0.0,
                        0.0, 0.0, -2.0/(far-near), 0.0,
                        -((left+right)/(right-left)),
                        -((top+bottom)/(top-bottom)),
                        -((far+near)/(far-near)),
                        1.0
                ];

    var orthoProj_P = gl.getUniformLocation(myShaderProgram, "orthoProj_M");
    gl.uniformMatrix4fv(orthoProj_P, false, orthoProj_M);
    
    // Set up perspective projection matrix P_persp using above planes

    perspProj_M = [
                        2.0*near/(right-left), 0.0, 0.0, 0.0,
                        0.0, 2*near/(top-bottom), 0.0, 0.0,
                        ((left+right)/(right-left)),
                        ((top+bottom)/(top-bottom)),
                        -((far+near)/(far-near)),
                        -1.0,
                        0.0, 0.0, -2.0*(far*near)/(far-near), 0.0
                        ];

    var perspProj_P = gl.getUniformLocation(myShaderProgram, "perspProj_M");
    gl.uniformMatrix4fv(perspProj_P, false, perspProj_M);
    
    // Use a flag to determine which matrix to send as uniform to shader
    // flag value should be changed by a button that switches between
    // orthographic and perspective projections
    
    projFlag_P = gl.getUniformLocation(myShaderProgram, "projFlag");

    var finalProj_P = gl.getUniformLocation(myShaderProgram, "finalProj_M");
    gl.uniformMatrix4fv(finalProj_P, false, perspProj_M);
}

function showOrthographic(){
	viewFlag = 0;
	gl.uniform1i(projFlag_P, viewFlag);
}

function showPerspective(){
	viewFlag = 1;
	gl.uniform1i(projFlag_P, viewFlag);
}

function transform(obj){
	scale(obj.scale);
	rotateX(obj.rotation.x);
	rotateY(obj.rotation.y);
	rotateZ(obj.rotation.z);
	translate(obj.translate);
}

function rotateX(angle){
	transRX.mat = [1.0,
          0.0,
          0.0,
          0.0,
          0.0,
          Math.cos(angle),
          -Math.sin(angle),
          0.0,
          0.0,
          Math.sin(angle),
          Math.cos(angle),
          0.0, 
          0.0, 
          0.0,
          0.0,
          1.0
         ];
    gl.uniformMatrix4fv(transRX.uni, false, transRX.mat);
}

function rotateY(angle) {
    transRY.mat = [Math.cos(angle),
          0.0,
          -Math.sin(angle),
          0.0,
          0.0,
          1.0,
          0.0,
          0.0,
          Math.sin(angle),
          0.0,
          Math.cos(angle),
          0.0, 
          0.0, 
          0.0,
          0.0,
          1.0
         ];
    gl.uniformMatrix4fv(transRY.uni, false, transRY.mat);
}

function rotateZ(angle) {
    transRZ.mat = [Math.cos(angle),
          -Math.sin(angle),
          0.0, 
          0.0,
          Math.sin(angle),
          Math.cos(angle),
          0.0,
          0.0,
          0.0,
          0.0,
          1.0,
          0.0, 
          0.0, 
          0.0,
          0.0,
          1.0
         ];
    gl.uniformMatrix4fv(transRZ.uni, false, transRZ.mat);
}

function translate(trans){
	transT.mat[12] = trans.x;
	transT.mat[13] = trans.y;
	transT.mat[14] = trans.z;
	gl.uniformMatrix4fv(transT.uni, false, transT.mat);
}

function scale(scaleFactor){
	transS.mat[0] = scaleFactor.x;
	transS.mat[5] = scaleFactor.y;
	transS.mat[10] = scaleFactor.z;
	gl.uniformMatrix4fv(transS.uni, false, transS.mat);
}