// Material.js

var specFlag = 1.0; // Flag Value 0=Off, 1=On
var specFlag_P; // Shader locatino of specular flag

function setObjectMaterial(material){
	setObjectAmbient(material.ambient);
	setObjectDiffuse(material.diffuse);
	setObjectSpecular(material.specular);
	setObjectShininess(material.shininess);

	var hasTexture_P = gl.getUniformLocation(myShaderProgram, "hasTexture");

	if(material.hasTexture){
		gl.uniform1i(hasTexture_P, 1);
		setTexture(material.textureID);
	} else gl.uniform1i(hasTexture_P, 0);
}

function setObjectAmbient(list){
	// Ambient Coefficient
    var ka_P = gl.getUniformLocation(myShaderProgram, "ka");
    gl.uniform3f(ka_P, list[0], list[1], list[2]);
}

function setObjectDiffuse(list){
	var kd_P = gl.getUniformLocation(myShaderProgram, "kd");
    gl.uniform3f(kd_P, list[0], list[1], list[2]);
}

function setObjectSpecular(list){
	var ks_P = gl.getUniformLocation(myShaderProgram, "ks");
    gl.uniform3f(ks_P, list[0], list[1], list[2]);
}

function setObjectShininess(alpha){
	var alpha_P = gl.getUniformLocation(myShaderProgram, "alpha");
    gl.uniform1f(alpha_P, alpha);
}

function setTexture(textureID){
	var myImage = document.getElementById(textureID);
    textureImage = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, textureImage);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, myImage);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
}

function toggleSpecular(){
	if(specFlag == 0.0) specFlag = 1.0;
	else if(specFlag == 1.0) specFlag = 0.0;
	gl.uniform1f(specFlag_P, specFlag);
}