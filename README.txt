Nicklas Kenyon
04-23-18

The image I used for a texture was found here: https://st2.depositphotos.com/1001214/5596/v/950/depositphotos_55965283-stock-illustration-floral-watercolor-pattern-texture-with.jpg

The code from Lab3 is modified to double the vertices other 
than the start and stop vertices, so that they can be assigned 
two different texture coordinates.

The majority of the implementation comes from the Lecture 11 slides.

The controls are the same as they were for Lab 3:

Q | W to rotate on X
A | S to rotate on Y
Z | X to rotate on Z
T | Y to scale on X
G | H to scale on Y
I | K to translate on Z
J | L to translate on X